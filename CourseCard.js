import React from 'react';

/*react-bootstrap components*/
import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function CourseCard(){
	
	return(

		<Row className="px-3" >
			<Col xs={12} md={4} lg={12} >
				<Card>
					<Card.Body>
						<Card.Title>Sample Course</Card.Title>
						<b>Description:</b>
						<p>This is a sample course offering.</p>
						<p><b>Price: </b> <br/> Php 40, 000</p>
		
				    	<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
			
		</Row>

		)
}



		